﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyAzureMVC.Startup))]
namespace MyAzureMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
